#include <iostream>
#include <fstream>

using namespace std;

ifstream f("date.in");
ofstream g("date.out");

struct tranz
{
    int starei,staref;
    char alfabet;
};

void citire_stari(int stari_initiale[100], int &n)
{
    int x;
    f>>n;
    for(int i = 0; i < n; i++)
    {
        f >> x;
        stari_initiale[i] = x;
    }
}

void citire_alfabet(char alfabet[100], int &n)
{
    char x;
    f >> n;
    for(int i = 0; i < n; i++)
    {
        f >> x;
        alfabet[i] = x;
    }
}

void citire_stare_initiala(int &n)
{
    f >> n;
}

void citire_tranzitii(tranz tranzitii[100],int &n)
{
    tranz x;
    f >> n;
    for(int i = 0; i < n; i++)
    {
        f >> x.starei >> x.alfabet >> x.staref;
        tranzitii[i] = x;
    }
}

void citire_stari_finale(int stari_finale[100], int &n)
{
    int x;
    f >> n;
    for(int i = 0; i < n; i++)
    {
        f >> x;
        stari_finale[i] = x;
    }
}

void minimizare(int stari[100], int nr_stari, int stari_finale[100], int nr_stari_finale, tranz tranzitii[100], int nr_tranzitii, char alfabet[100], int nr_alfabet)
{
    int automat[100][100];
    for(int i = 0; i < nr_stari; i++)
        for(int j = 0; j < nr_stari; j++)
        {
            if(j >= i)
                automat[i][j] = -1;
            else
                automat[i][j] = 0;
        }

    char e;
    for(int i = 0; i < nr_stari; i++)
        for(int j = 0; j < i; j++)
        {
            e=0;
            for(int k = 0; k < nr_stari_finale; k++)
                if(stari_finale[k] == i || stari_finale[k] == j)
                    e++;
            if(e == 1)
                automat[i][j] = 1;
        }

    int marcat;
    do
    {
        marcat = 0;
        for(int i = 0; i < nr_stari; i++)
            for(int j = 0; j < i; j++)
            {
                if(automat[i][j] == 0)
                {
                    for(int k = 0; k < nr_alfabet; k++)
                    {
                        int perechea1, perechea2;
                        char litera;
                        litera = alfabet[k];
                        for(int x = 0; x < nr_tranzitii; x++)
                        {
                            if(tranzitii[x].alfabet == litera && tranzitii[x].starei == i)
                                perechea1 = tranzitii[x].staref;
                            if(tranzitii[x].alfabet == litera && tranzitii[x].starei == j)
                                perechea2 = tranzitii[x].staref;
                        }
                        if(automat[perechea1][perechea2] == 1 || automat[perechea2][perechea1] == 1)
                        {
                            automat[i][j] = 1;
                            marcat = 1;
                            break;
                        }

                    }
                }
            }

    }while(marcat == 1);

    int automat_min[100][100], f[100], ii, jj, perechea1, perechea2;
    for (int i = 0; i < 100; i++)
        f[i] = 0;

    for(int i = 0; i < nr_stari; i++)
        for(int j = 0; j < nr_stari; j++)
            automat_min[i][j] = -1;

    for(int i = 0; i < nr_stari; i++)
        for(int j = 0; j < i; j++)
        {
            e = 0;
            if(automat[i][j] == 0)
            {
                ii = 0;
                jj = 0;
                perechea1 = -1;
                perechea2 = -1;
                for(ii = 0; ii < nr_stari && automat_min[ii][0] != -1; ii++)
                {
                    for(jj = 0; jj < nr_stari && automat_min[ii][jj] != -1; jj++)
                    {
                        if(automat_min[ii][jj] == i)
                        {
                            perechea1 = j;
                            e = 1;
                        }
                        if(automat_min[ii][jj] == j)
                        {
                            perechea2 = i;
                            e = 1;
                        }
                    }
                    if(perechea1 != -1)
                    {
                        automat_min[ii][jj] = perechea1;
                        f[perechea1]++;
                    }
                    else if (perechea2 != -1)
                    {
                        automat_min[ii][jj] = perechea2;
                        f[perechea2]++;
                    }
                    else
                        continue;
                }
                if(e == 0)
                {
                    automat_min[ii][0] = i;
                    automat_min[ii][1] = j;
                    f[i]++;
                    f[j]++;
                }
            }
        }

    for(int i = 0; i < nr_stari; i++)
        if(f[i] == 0)
        {
            automat_min[ii][0] = i;
            ii++;
        }

    int finale[100]={0}, i1;
    for(int i = 0; i < nr_stari && automat_min[i][0] != -1; i++)
        for(int j = 0; j < nr_stari && automat_min[i][j] != -1; j++)
            for(int k = 0; k < nr_stari_finale; k++)
                if(automat_min[i][j] == stari_finale[k])
                    finale[i] = 1;

    g << "Starile noi sunt: ";
    for(int i = 0; i < nr_stari && automat_min[i][0] != -1; i++)
    {
        for(int j = 0; j < nr_stari && automat_min[i][j] != -1; j++)
            g << automat_min[i][j];
        g << "    ";
    }

    g << endl << "Starile finale sunt: ";
    for(int i = 0; i < nr_stari && automat_min[i][0] != -1; i++)
    {
        for(int j = 0; j < nr_stari && automat_min[i][j] != -1; j++)
            if(finale[i] == 1)
                g << automat_min[i][j];
        g << "    ";
    }

    g << endl << "Noile tranzitii sunt: ";
    for(int i = 0; i < nr_stari; i++)
        for(int k = 0; k < nr_alfabet; k++)
            for(int j = 0; j < nr_tranzitii; j++)
                if(automat_min[i][0] == tranzitii[j].starei && alfabet[k] == tranzitii[j].alfabet)
                {
                    for(int x = 0; automat_min[i][x] != -1; x++)
                        g << automat_min[i][x];
                    g << alfabet[k];
                    e = 0;
                    for(i1 = 0; i1 < nr_stari && automat_min[i1][0] != -1 && e == 0; i1++)
                        for(int j1 = 0; j1 < nr_stari && automat_min[i1][j1] != -1; j1++)
                            if(automat_min[i1][j1] == tranzitii[j].staref)
                            {
                                e = 1;
                                break;
                            }
                    for(int j1 = 0; automat_min[i1-1][j1] != -1; j1++)
                        g << automat_min[i1-1][j1];
                    g << "    ";
                    break;
                }
}

int nr_stari_initiale, nr_alfabet, stare_initiala, nr_tranzitii, nr_stari_finale;
int stari[100], stari_finale[100];
char alfabet[100];
tranz tranzitii[100];

int main()
{
    citire_stari(stari, nr_stari_initiale);
    citire_stare_initiala(stare_initiala);
    citire_stari_finale(stari_finale, nr_stari_finale);
    citire_alfabet(alfabet, nr_alfabet);
    citire_tranzitii(tranzitii, nr_tranzitii);
    minimizare(stari, nr_stari_initiale, stari_finale, nr_stari_finale, tranzitii, nr_tranzitii, alfabet, nr_alfabet);
}
